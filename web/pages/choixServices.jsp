<%--
  Created by IntelliJ IDEA.
  User: itu
  Date: 19/01/2023
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@page import="java.sql.Connection"%>
<%@page import="connect.ConnectionDB"%>
<%@page import="table.Service"%>
<%@page import="java.util.Vector"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <link href="../assets/css/page.css" rel="stylesheet" type="text/css">
    <title>Imprimerie</title>
</head>
<body>
    <div id="content">
        <div id="menu">
            <form action="./revenuparHeure.jsp" method="">
                <select name="service" class="liste-der">
                    
                    <%
                        Connection c=ConnectionDB.makaConPsql();
                        Vector<Service> ls=Service.getAllService(c);
                        for(int i=0;i<ls.size();i++)
                        {
                    %>
                    <option value=<% out.print(ls.get(i).getIdService()); %>><% out.print(ls.get(i).getNomService()); %></option>
        
                    <% } %>

                </select>
                <input type="submit" value="ok">
            </form>
        </div>
        <br>
        <div class="quit-div">
            <a href="../index.jsp"><div class="quit-bout">retour</div></a>
        </div>
    </div>
</body>
</html>
