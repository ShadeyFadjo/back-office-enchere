/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Best
 */
public class Dbconnect {
    private String user="s5";
    private String pwd="s5";
    private String dbname="enchere";

    public void setuser(String user)
	{
		this.user=user;
	}
	public void setdbname(String dbn)
	{
		this.dbname=dbn;
	}

	public void setpwd(String pwd)
	{
		this.pwd=pwd;
	}

	public String getdbname()
	{
		return this.dbname;
	}

    public String getpwd()
	{
		return this.pwd;
	}

	public String getuser()
	{
		return this.user;
	}

    public Connection getConnect()
    {
        Connection ret=null;
        try{
            Class.forName("org.postgresql.Driver");
            ret=DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+getdbname(),getuser(),getpwd());
        }
        catch(ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
        finally{return ret;}
    }
}
